package calc

func Add(numbers ...int) int {
	sum := 0
   for _, j := range numbers {
      sum = sum + j
   }

   return sum
}

func Subtract(numbers ...int) int {
   result := numbers[0]
   for _, j := range numbers[1:] {
      result = result - j
   }
   return result
}

func Divide(num1 int, num2 int) float64 {
   return float64(num1) / float64(num2)
}
